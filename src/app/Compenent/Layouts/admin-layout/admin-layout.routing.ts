import { Routes } from '@angular/router';
import { DashbordComponent } from '../../Dashbord/dashbord/dashbord.component';
import { AjoutePersonneComponent } from '../../Personne/ajoute-personne/ajoute-personne.component';
import { ListePersonneComponent } from '../../Personne/liste-personne/liste-personne.component';
import { AjoutePesageComponent } from '../../Pesage/ajoute-pesage/ajoute-pesage.component';
import { ListePesageComponent } from '../../Pesage/liste-pesage/liste-pesage.component';
import { ModifierPesageComponent } from '../../Pesage/modifier-pesage/modifier-pesage.component';
import { ListePesageTotalComponent } from '../../Pesage/liste-pesage-total/liste-pesage-total.component';
import { ModifierPersonneComponent } from '../../Personne/modifier-personne/modifier-personne.component';

export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard',      component: DashbordComponent },
    { path: 'ajoute-personne',   component: AjoutePersonneComponent },
    { path: 'personne', component:ListePersonneComponent},
    { path: 'ajoute-pesage', component:AjoutePesageComponent},
    { path: 'pesage', component:ListePesageComponent},
    { path: 'modifier-pesage', component:ModifierPesageComponent},
    { path: 'modifier-personne/:id', component:ModifierPersonneComponent},
    { path: 'total-pesage', component:ListePesageTotalComponent}

];
