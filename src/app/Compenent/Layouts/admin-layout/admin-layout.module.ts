import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { AjoutePersonneComponent } from '../../Personne/ajoute-personne/ajoute-personne.component';
import { ListePersonneComponent } from '../../Personne/liste-personne/liste-personne.component';
import { ModifierPersonneComponent } from '../../Personne/modifier-personne/modifier-personne.component';
import { ModifierPesageComponent } from '../../Pesage/modifier-pesage/modifier-pesage.component';
import { AjoutePesageComponent } from '../../Pesage/ajoute-pesage/ajoute-pesage.component';
import { ListePesageComponent } from '../../Pesage/liste-pesage/liste-pesage.component';
import { ActionPesageComponent } from '../../Pesage/action-pesage/action-pesage.component';
import { ListePresenceComponent } from '../../Presence Travail/liste-presence/liste-presence.component';
import { TotalPresenceComponent } from '../../Presence Travail/total-presence/total-presence.component';
import { LoginComponent } from '../../Auth/login/login.component';
import { DashbordComponent } from '../../Dashbord/dashbord/dashbord.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { FooterComponent } from '../footer/footer.component';
import { MaterialModule } from 'src/app/Core/Material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdbBootstrapModule } from 'src/app/Core/Mdboostrap/mdboostrap.module';
import { ListePesageTotalComponent } from '../../Pesage/liste-pesage-total/liste-pesage-total.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule,
      ReactiveFormsModule,
      MaterialModule,
      HttpClientModule,
      FormsModule,
      MdbBootstrapModule,
      RouterModule.forChild(AdminLayoutRoutes),
    ],
    declarations: [
        AjoutePersonneComponent,
        ListePersonneComponent,
        ModifierPersonneComponent,
        ModifierPesageComponent,
        AjoutePesageComponent,
        ListePesageComponent,
        ActionPesageComponent,
        ListePresenceComponent,
        TotalPresenceComponent,
        // LoginComponent,
        DashbordComponent,
        NavbarComponent,
        SidebarComponent,
        FooterComponent,
        ListePesageTotalComponent
    ],
    exports: [
      CommonModule,
      RouterModule,
      ReactiveFormsModule,
      MaterialModule,
      HttpClientModule,
      FormsModule,
      MdbBootstrapModule,

      AjoutePersonneComponent,
      ListePersonneComponent,
      ModifierPersonneComponent,
      ModifierPesageComponent,
      AjoutePesageComponent,
      ListePesageComponent,
      ActionPesageComponent,
      ListePresenceComponent,
      TotalPresenceComponent,
      // LoginComponent,
      DashbordComponent,
      NavbarComponent,
      SidebarComponent,
      FooterComponent,
      ListePesageTotalComponent
    ]
  })
  export class AdminLayoutModule {}
