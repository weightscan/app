import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Core/Services/Auth/auth.service';
import { Login } from 'src/app/Shared/Models/login';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {

  constructor(private router:Router,private  serviceAuth:AuthService) { }

  ngOnInit() {
    // this.serviceAuth.getAllLogin().subscribe(
    //   res=>{
    //     console.log(res);
    //     if(res){
    //       this.router.navigate(['/login']);
    //     }
    //   },
    //   err=>{
    //     console.log(err);
    //   }
    // );
  }

}
