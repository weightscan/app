import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Core/Services/Auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private router:Router,private serviceAuth:AuthService) { }

  ngOnInit() {
    
  }

  logOut(){

    this.serviceAuth.deleteAllLogin().subscribe(
      res=>{
        console.log(res,"logout")
        this.router.navigate(['/login']);
      },
      err=>{
        console.log(err);
      }
    );
  }
}
