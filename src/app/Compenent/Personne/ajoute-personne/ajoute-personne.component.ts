import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PersonneService } from 'src/app/Core/Services/Personne/personne.service';
import { Personne } from 'src/app/Shared/Models/personne';

@Component({
  selector: 'app-ajoute-personne',
  templateUrl: './ajoute-personne.component.html',
  styleUrls: ['./ajoute-personne.component.scss']
})
export class AjoutePersonneComponent implements OnInit {

  constructor(private builder: FormBuilder, private servicePersonne: PersonneService,) { }
  formGroupePersonne:FormGroup;personne:Personne;
  ngOnInit() {

    this.formGroupePersonne = this.builder.group({
      id: new FormControl(0),
      matricule: new FormControl(),
      nom: new FormControl(),
      prenom: new FormControl(),
      phone: new FormControl(),
      cin: new FormControl(),
      cnss: new FormControl(),
      dateNaissance: new FormControl(),
      situation: new FormControl(),
      ville: new FormControl(),
      adresse: new FormControl(),
      nombreEnfant: new FormControl(0),
      dateEmbauche: new FormControl(),
      dateArret: new FormControl(),
      idBadge: new FormControl(null)
    });
  }
  ajoutePersonne(){
    this.personne=this.formGroupePersonne.value;
    console.log(this.personne);
    this.servicePersonne.postPersonne(this.personne).subscribe(
      res=>{
        console.log('ajoute personne avec succes',res);
      },
      err=>{
        console.log(err);
      }
    );
  }

}
