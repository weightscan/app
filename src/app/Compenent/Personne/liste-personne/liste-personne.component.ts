import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Personne } from 'src/app/Shared/Models/personne';
import { PersonneService } from 'src/app/Core/Services/Personne/personne.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-personne',
  templateUrl: './liste-personne.component.html',
  styleUrls: ['./liste-personne.component.scss']
})
export class ListePersonneComponent implements OnInit {

  displayedColumns: string[] = ['matricule', 'nomComplet', 'prenom', 'cin','badge', 'cnss', 'phone', 'actions'];
  dataSource;
  constructor(
    private service: PersonneService,private router:Router
  ) { }

  ngOnInit() {
    this.service.getPersonne().subscribe(
      res => {
        console.log(res);
        this.dataSource = new MatTableDataSource(res as any[]);
      },
      err => {
        console.log(err);
      }
    );
  }

  onUpdatePersonne(id){
    this.router.navigate(['/modifier-personne',id]);
  }
}
