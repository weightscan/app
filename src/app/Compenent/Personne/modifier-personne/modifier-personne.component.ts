import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonneService } from 'src/app/Core/Services/Personne/personne.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { BadgeService } from 'src/app/Core/Services/Badge/badge.service';
import { Personne } from 'src/app/Shared/Models/personne';

@Component({
  selector: 'app-modifier-personne',
  templateUrl: './modifier-personne.component.html',
  styleUrls: ['./modifier-personne.component.scss']
})
export class ModifierPersonneComponent implements OnInit {

  idPersonne:number;personnes:Personne[];personne:Personne;formGroupePersonne:FormGroup;

  constructor(
    private route: ActivatedRoute,private servicePersonne:PersonneService,private builder:FormBuilder,
    private serviceBadge:BadgeService,private router:Router
    ) { }


  ngOnInit() {
    this.formGroupePersonne = this.builder.group({
      // id: new FormControl(this.personne.id),
      matricule: new FormControl(),
      nom: new FormControl(),
      prenom: new FormControl(),
      phone: new FormControl(),
      cin: new FormControl(),
      cnss: new FormControl(),
      dateNaissance: new FormControl(),
      situation: new FormControl(),
      ville: new FormControl(),
      adresse: new FormControl(),
      nombreEnfant: new FormControl(),
      dateEmbauche: new FormControl(),
      dateArret: new FormControl(),
      idBadge: new FormControl()
    });

    this.servicePersonne.getPersonne().subscribe(
      res => {
        var personnes=res as Personne[];
        // console.log(personnes);
        var personne:Personne=personnes.find(p=> p.id === 2733);
        console.log(personne);
        this.remplirForm(personne);
      },
      err => {
        console.log(err);
      }
    );

    this.idPersonne=parseInt(this.route.snapshot.params['id']);

    // console.log(this.personnes);

    // this.personne=this.personnes.find(p=> p.id == 2733);

    // console.log(this.personne);


  }

  remplirForm(_personne:Personne){
    if(_personne != null)
    {
      this.formGroupePersonne = this.builder.group({
        id: new FormControl(_personne.id as number),
        matricule: new FormControl(_personne.matricule),
        nom: new FormControl(_personne.nom),
        prenom: new FormControl(_personne.prenom),
        phone: new FormControl(_personne.phone),
        cin: new FormControl(_personne.cin),
        cnss: new FormControl(_personne.cnss),
        dateNaissance: new FormControl(_personne.dateNaissance),
        situation: new FormControl(_personne.situation),
        ville: new FormControl(_personne.ville),
        adresse: new FormControl(_personne.adresse),
        nombreEnfant: new FormControl(_personne.nombreEnfant),
        dateEmbauche: new FormControl(_personne.dateEmbauche),
        dateArret: new FormControl(_personne.dateArret),
        idBadge: new FormControl(_personne.idBadge as number)
      });
    }
  }
  modifierPersonne(){
    // this.formGroupePersonne.controls.idBadge.patchValue()
    var personne:Personne=this.formGroupePersonne.value;

    var id:number=personne.id as number;
    
    this.servicePersonne.putPersonne(personne,id).subscribe(
      res=>{
        console.log('modifier avec succes');
        this.router.navigate(['/personne']);
      },
      err=>{
        console.log(err);
      }
    );
  }

}
