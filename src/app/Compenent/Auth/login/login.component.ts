import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Core/Services/User/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/Shared/Models/user';
import { AuthService } from 'src/app/Core/Services/Auth/auth.service';
import { Login } from 'src/app/Shared/Models/login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  notExist;username:string;password:string;
  _login={
    id: 1,
    idUser: 1,
    logOut: true,
    dateLogin: '2020/03/09',
    dateLogout: '2020/03/09'
  };
  constructor(private serivceUser:UserService,private router:Router,
    private serviceAuth:AuthService) { }

  ngOnInit() {
    this.serviceAuth.deleteAllLogin().subscribe(
      res=>{
        // console.log(res);
      },
      err=>{
        console.log(err);
      }
    );
  }

  login(){
    this.serivceUser.getUser(this.username,this.password).subscribe(
      res=>{
        if(res == true){

          // console.log(res);
          // console.log(this._login);
          this.serviceAuth.postLogin(this._login).subscribe(
            result=>{
              // console.log(result);
              this.router.navigate(['/dashboard']);
              // this.notExist=true;
            },
            error=>{
              console.log(error);
            }
          );

        }
        else if(res == false)
        {
          this.username='';
          this.password='';
          this.notExist=false;
        }
        console.log(res);

      },err=>{
        console.log(err);
      }
    );
  }
}
