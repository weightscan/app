import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ajoute-pesage',
  templateUrl: './ajoute-pesage.component.html',
  styleUrls: ['./ajoute-pesage.component.scss']
})
export class AjoutePesageComponent implements OnInit {

  constructor(private builder:FormBuilder) { }

  formGroupePesage:FormGroup;
  serieNumber=new FormControl();
  ngOnInit() {
      // this.serieNumber=new FormControl();
      this.formGroupePesage=this.builder.group(
      {
        id: new FormControl(0),
        idPersonne: new FormControl(1),
        poid: new FormControl(),
      }
    ) 
  }

  ajoutePesage(){
    console.log('pesage',this.formGroupePesage.value);
    console.log('serie number',this.serieNumber);
  }
}
