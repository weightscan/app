import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PesageService } from 'src/app/Core/Services/Pesage/pesage.service';
import { MatTableDataSource } from '@angular/material';
import { Pesage } from 'src/app/Shared/Models/pesage';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { ExcelServicesService } from 'src/app/Core/Services/Excel/excel-services.service';

@Component({
  selector: 'app-liste-pesage',
  templateUrl: './liste-pesage.component.html',
  styleUrls: ['./liste-pesage.component.scss']
})
export class ListePesageComponent implements OnInit,AfterViewInit {

  displayedColumns: string[] = ['matricule', 'nom', 'prenom', 'date', 'heure', 'poid','actions'];
  dataSource;dateDebut;dateFin;matricule;data=[];

  constructor(private servicePesage:PesageService, private serviceExcel:ExcelServicesService) { }

  ngOnInit() {
    this.servicePesage.getPesageToday().subscribe(
      res=>{
        this.dataSource=new MatTableDataSource(res as Pesage[]);
        this.data=res as any[];
      },
      err=>{
        console.log(err);
      }
    );
    this.dateDebut=moment(new Date()).format('YYYY-MM-DD'+'T00:00:00');
    this.dateFin=moment(new Date()).format('YYYY-MM-DD'+'T23:59:59');
  }
  ngAfterViewInit(){
    this.dateDebut=moment(new Date()).format('YYYY-MM-DD'+'T00:00:00');
    this.dateFin=moment(new Date()).format('YYYY-MM-DD'+'T23:59:59');
  }
  onSearch(){
    var db:string=moment(this.dateDebut).format('YYYY-MM-DD'+'T00:00:00');
    var df:string=moment(this.dateFin).format('YYYY-MM-DD'+'T23:59:59');
    var matricule:string=matricule;
    if(this.matricule==null && this.dateDebut!=null && this.dateFin!=null){
      this.servicePesage.getPesagePeriod(db,df).subscribe(
        res=>{
          this.dataSource=new MatTableDataSource(res as any[]);
          this.data=res as any[];
        },
        err=>{
          console.log(err);
        }
      );
    }
    if(this.matricule!=null && this.dateDebut!=null && this.dateFin!=null){
      this.servicePesage.getPesagePeriodPersonne(matricule,db,df).subscribe(
        res=>{
          this.dataSource=new MatTableDataSource(res as any[]);
          this.data=res as any[];
        },
        err=>{
          console.log(err);
        }
      );
    }
  }
  exportExcel(){
    this.serviceExcel.exportAsExcelFile(this.data,'simple');
  }

}
