import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PesageService } from 'src/app/Core/Services/Pesage/pesage.service';
import { MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { ExcelServicesService } from 'src/app/Core/Services/Excel/excel-services.service';

@Component({
  selector: 'app-liste-pesage-total',
  templateUrl: './liste-pesage-total.component.html',
  styleUrls: ['./liste-pesage-total.component.scss']
})
export class ListePesageTotalComponent implements OnInit,AfterViewInit {

  displayedColumns: string[] = ['matricule', 'nom', 'prenom','presence', 'poid','mantant'];
  dataSource;dateDebut;dateFin;matricule;data=[];

  constructor(private servicePesage:PesageService, private serviceExcel:ExcelServicesService) { }

  ngOnInit() {
    this.servicePesage.getPesageTotal().subscribe(
      res=>{
        this.dataSource=new MatTableDataSource(res as any[]);
        this.data=res as any[];
      },
      err=>{
        console.log(err);
      }
    );
    this.dateDebut=moment(new Date()).format('YYYY-MM-DD'+'T00:00:00');
    this.dateFin=moment(new Date()).format('YYYY-MM-DD'+'T23:59:59');
  }

  ngAfterViewInit(){
    this.dateDebut=moment(new Date()).format('YYYY-MM-DD'+'T00:00:00');
    this.dateFin=moment(new Date()).format('YYYY-MM-DD'+'T23:59:59');
  }
  onSearch(){
    var db:string=moment(this.dateDebut).format('YYYY-MM-DD'+'T00:00:00');
    var df:string=moment(this.dateFin).format('YYYY-MM-DD'+'T23:59:59');
    var matricule:string=this.matricule;

    if(this.matricule!=null && this.dateDebut!=null && this.dateFin!=null){
      this.servicePesage.getTotalPesagePeriodPersonne(matricule,db,df).subscribe(
        res=>{
          this.dataSource=new MatTableDataSource(res as any[]);
          this.data=res as any[];
        },
        err=>{
          console.log(err);
        }
      );
    }
    if(this.matricule==null && this.dateDebut!=null && this.dateFin!=null){
      this.servicePesage.getTotalPesagePeriod(db,df).subscribe(
        res=>{
          this.dataSource=new MatTableDataSource(res as any[]);
          this.data=res as any[];
        },
        err=>{
          console.log(err);
        }
      );
    }
  }
  exportExcel(){
    this.serviceExcel.exportAsExcelFile(this.data,'simple');
  }
  getTotalPesagePeriodPersonne(matricule,db,df){
      return this.data.filter(function(d){
        return d.matricule==matricule;
      });
  }
  getTotalPesagePeriod(db,df){

  }
}
