export class User {
    id: number;
    nom: string;
    prenom: string;
    userName: string;
    password: string;
    phone: string;
    role: string;
}
