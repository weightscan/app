export class Login {
    id: number;
    idUser: string;
    logOut: string;
    dateLogin: string;
    dateLogout: string;
}
