import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './Compenent/Layouts/admin-layout/admin-layout.component';
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { LoginComponent } from './Compenent/Auth/login/login.component';


const routes: Routes =[
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: [{
      path: '',
      loadChildren: './Compenent/Layouts/admin-layout/admin-layout.module#AdminLayoutModule'
    }]
  },{
    path:'login',
    component:LoginComponent,
    pathMatch:'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,

    RouterModule.forRoot(routes,{
      //  useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }

