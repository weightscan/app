import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './Core/Services/Auth/auth.service';
import { Login } from './Shared/Models/login';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'WeightScanApp';

  constructor(private router:Router,private  serviceAuth:AuthService){

  }
  ngOnInit(){

    this.serviceAuth.getAllLogin().subscribe(
      res=>{
        // console.log(res);
        if(res=null){
          console.log('rien login' );
          this.router.navigate(['/login']);
        }
      },
      err=>{
        console.log(err);
      }
    );
    // this.router.navigate(['']);
  }
}
