import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsModule, WavesModule, CollapseModule } from 'angular-bootstrap-md';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ButtonsModule,
    WavesModule,
    CollapseModule
  ],
  exports: [
    CommonModule,
    ButtonsModule,
    WavesModule,
    CollapseModule
  ]
})
export class MdbBootstrapModule { }
