
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { constant } from 'src/app/Shared/constant';

const API = constant.url + 'pesages';
@Injectable({
  providedIn: 'root'
})
export class PesageService {

  constructor(private http: HttpClient) { }

  //CRUD Pesage
  getPesage() {
    return this.http.get(`${API}`);
  }
  getPesageToday(){
    return this.http.get<any>(`${API}/today`);
  }
  getAllPesages() {
    return this.http.get<any>(`${API}/all`);
  }
  //
  getPesageTotal() {
    return this.http.get(`${API}/get`);
  }
  getPesagePeriod(db, df) {
    return this.http.get(`${API}/` + db + "/" + df);
  }
  getPesagePeriodPersonne(mat, db, df) {
    return this.http.get(`${API}/` +  mat + "/" + db + "/" + df);
  }
  getTotalPesagePeriod(db, df) {
    return this.http.get(`${API}` + "/du/" + db + "/au/" + df);
  }
  getTotalPesagePeriodPersonne(mat, db, df) {
    return this.http.get(`${API}` + "/matricule/" + mat + "/du/" + db + "/au/" + df);
  }

  postPesage(data) {
    return this.http.post(`${API}`, data);
  }
  putPesage(id, data) {
    return this.http.put(`${API}` + '/' + id, data)
  }

  deletePesage(id) {
    return this.http.delete(`${API}` + '/' + id);
  }
}
