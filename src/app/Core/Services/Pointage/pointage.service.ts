
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { constant } from 'src/app/Shared/constant';

const API = constant.url + 'pointages';
@Injectable({
  providedIn: 'root'
})
export class PointageService {

  constructor(private http: HttpClient) { }

  //Pointage
  postPointage(data) {
    return this.http.post(`${API}`, data);
  }

  getAllPointages() {
    return this.http.get<any>(`${API}/all`);
  }
  getPointage() {
    return this.http.get(`${API}`);
  }
  getPointagesToday() {
    return this.http.get(`${API}` + '/today');
  }
  getPointgesPeriod(db, df) {
    return this.http.get(`${API}` + '/du/' + db + '/au/' + df);
  }
  getPointgesPeriodPersonne(mat, db, df) {
    return this.http.get(`${API}` + '/matricule/' + mat + '/du/' + db + '/au/' + df);
  }
  getPointagesPersonne(matricule: string) {
    return this.http.get(`${API}` + '/matricule/' + matricule);
  }
  getTotalPointage(db, df) {
    return this.http.delete(`${API}` + '/totalPresence/' + db + '/' + df);
  }
  deletepointage(id) {
    return this.http.delete(`${API}` + '/' + id);
  }
  putPointage(id, data) {
    return this.http.put(`${API}` + '/' + id, data)
  }

  //   getAllPointages() {
  //     return this.http.get<any>(`${API}all`)
  //       .pipe(
  //         tap(_ => this.log('get all sous-traitant')),
  //         catchError(this.handleError('get all sous-traitant', []))
  //       );

  //  }
}
