import { Injectable } from '@angular/core';
import { constant } from 'src/app/Shared/constant';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/Shared/Models/user';
import { Login } from 'src/app/Shared/Models/login';

const API = constant.url + 'logins';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // username:string=null;
  id: number;
  connect: boolean;

  constructor(private http: HttpClient) { }


  getAllLogin(){
    return this.http.get(`${API}`);
  }
  postLogin(data){
    return this.http.post(`${API}`,data);
  }
  deleteLogin(id){
    return this.http.delete(`${API}/`+id);
  }
  deleteAllLogin(){
    return this.http.delete(`${API}`);
  }
  lastLogin() {
    return this.http.get(`${API}` + '/lastLogin');
  }
}
