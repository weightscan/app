import { Injectable } from '@angular/core';
import { constant } from 'src/app/Shared/constant';
import { HttpClient } from '@angular/common/http';
const API = constant.url + 'badges';

@Injectable({
  providedIn: 'root'
})
export class BadgeService {

  constructor(private http:HttpClient) { }

  getNumeroBadge() {
    return this.http.get(`${API}/numbadges`);
  }
}
