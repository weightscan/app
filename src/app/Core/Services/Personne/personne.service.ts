import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { constant } from 'src/app/Shared/constant';

const API = constant.url + 'personnes';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {


  constructor(private http: HttpClient) {
  }


  getPersonne() {
    return this.http.get(`${API}/alls`);
  }
  getOnePersonne(matricule: string) {
    return this.http.get(`${API}` + '/matricule/' + matricule)
  }
  postPersonne(data) {
    return this.http.post(`${API}`, data);
  }

  putPersonne(data, id) {
    return this.http.put(`${API}` + '/' + id, data);
  }

  deletePersonne(id) {
    return this.http.delete(`${API}` + '/' + id);
  }
}
