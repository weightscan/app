import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { constant } from 'src/app/Shared/constant';
import { HttpClient } from '@angular/common/http';

const API = constant.url + 'users';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {
  }

  // getUser() {
  //   return this.http.get(`${API}`);
  // }

  getAllUser() {
    return this.http.get(`${API}`);
  }

  postUser(data) {
    return this.http.post(`${API}`, data);
  }

  putUser(data, id) {
    return this.http.put(`${API}` + '/' + id, data);
  }

  deleteUser(id) {
    return this.http.delete(`${API}` + '/' + id);
  }
  
  getUser(username: string, password: string) {
    return this.http.get(`${API}` + '/userexists/' + username + '/' + password);
  }
}
